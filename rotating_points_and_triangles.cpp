#include <iostream>
#include <cmath>
using namespace std;

struct Punkt {
    double x, y;
};

struct Trojkat {
    Punkt A, B, C;
};

void P_info(const Punkt*);
void T_info(const Trojkat*);
void rot(Punkt*,double);
void rot(Trojkat*,double);

int main(void) {
    Punkt A;
    A.x = -1;
    A.y =  2;

    Punkt B = { -1, 1 };

    Punkt C = { 2 };
    C.y = -1;

    Trojkat T = { A, B };
    T.C = C;
    /*
     * UWAGA: W MS VC++ 6 powyzsza forma inicjalizacji
     * obiektu T nie dziala. Mozna uzyc:
     *    Trojkat T;
     *    T.A = A;
     *    T.B = B;
     *    T.C = C;
     */

    cout << "\nWyjsciowe punty: ";
    P_info(&A); P_info(&B); P_info(&C);
    cout << endl;

    cout << "Trojkat: ";
    T_info(&T);

    rot(&A,90); rot(&B,90); rot(&C,90);
    cout << "\nPunkty A, B, C po obrocie o 90 stopni: ";
    P_info(&A); P_info(&B); P_info(&C);
    cout << endl;

    cout << "Trojkat: ";
    T_info(&T);

    rot(&T,90); rot(&T,90);
    cout << "\nT po obrocie dwa razy o  90 stopni: ";
    T_info(&T);

    rot(&T,180);
    cout << "\nT po obrocie o nastepne 180 stopni: ";
    T_info(&T);
}

void P_info(const Punkt* pP) {
    cout << "(" << pP->x << ", " << pP->y << ") ";
}

void T_info(const Trojkat* pT) {
    cout << "A="; P_info(&pT->A);
    cout << "B="; P_info(&pT->B);
    cout << "C="; P_info(&pT->C);
    cout << endl;
}

void rot(Punkt* pP, double phi) {
    static double conver = atan(1.)/45;
    phi = phi*conver; // stopnie -> radiany

    double c = pP->x;
    pP->x = pP->x * cos(phi) - pP->y * sin(phi);
    pP->y =     c * sin(phi) + pP->y * cos(phi);
}

void rot(Trojkat* pT, double phi) {
    rot( &pT->A, phi);
    rot( &pT->B, phi);
    rot( &pT->C, phi);
}
